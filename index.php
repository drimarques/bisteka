<?php 
	date_default_timezone_set('Brazil/East');
	date_default_timezone_set('America/Sao_Paulo');
	require_once "php/classes/RegistraContagemAcessos.php"
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Bisteka</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<style type="text/css">
		* {
			margin: 0;
		}
		html, body {
			width: 100%;
			height: 100%;
		}
		body {
			background-color: rgba(0, 0, 0, 0.1);
		}
		img {
			width: auto;
			height: auto;
			position: fixed;
			left: 50%;
			margin-left: -275px;
			top: 50%;
			margin-top: -183px;
		}
	</style>
</head>
<body>
	<img src='bisteka.jpg' alt='Bisteka'>
</body>
</html>