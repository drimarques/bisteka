<?php 
function carregaClasse($arquivo){
	if(file_exists("php/classes/".$arquivo.".php")){
		require_once("php/classes/".$arquivo.".php");
	}
}
spl_autoload_register("carregaClasse");

$dao = new AcessosDAO();
$qtdAcessos = $dao->registraAcesso();

$mail = new EnvioEmail();
$mail->adicionaDestinatario("danilo.parreira@gmail.com");
$mail->adicionaCopiaOculta("adrianomfpassos@gmail.com");

$mail->setAssunto("Acesso Bisteka");
$mail->setFrom("Bisteka");

$dataAtual = date("d/m/Y H:i:s");
$mail->setMensagem("Olá Danilo <br>
					Tivemos um acesso em seu domínio bisteka.com.br em {$dataAtual}. <br>
					Quantidade de acessos: <b>{$qtdAcessos}</b>.
				");

$mail->enviaEmail();
